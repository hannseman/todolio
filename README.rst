=======
Todolio
=======

A Todo-list.

-------------
Documentation
-------------
Todolio requires a redis-server running on localhost:6379 and python 3.4.

To run locally,

.. code-block:: bash

    pip install -r requirements.txt
    python manage.py runserver


To run the tests,

.. code-block:: bash

    python manage.py test

