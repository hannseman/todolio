import json
import unittest
from todolio.app import create_app
from todolio.settings import TestConfig
from todolio.task import services
from todolio.task.views import get_task_list_key

app = create_app(TestConfig)


class TestTaskREST(unittest.TestCase):
    TASK_LIST_URL = '/tasks'
    TASK_URL = '/tasks/{}'

    def setUp(self):
        self.client = app.test_client()

    def tearDown(self):
        with app.test_request_context():
            services.delete_tasks(get_task_list_key())

    def create_task(self, list_key, task='testing stuff', position=1):
        return services.create_task(list_key, {'task': task, 'position': position})

    def get_json_response(self, response_data):
        return json.loads(response_data.decode('utf-8'))

    def test_get_tasks(self):
        # Test empty response
        with app.test_request_context(), app.test_client() as c:
            response = c.get(self.TASK_LIST_URL)
            assert response.status_code == 200
            # Test response with tasks
            list_key = get_task_list_key()
            self.create_task(list_key), self.create_task(list_key)
            response = c.get(self.TASK_LIST_URL)
            assert response.status_code == 200
            tasks = self.get_json_response(response.data)
            assert len(tasks) == 2

    def test_create_task(self):
        with app.test_client() as c:
            data = {'task': 'tästing', 'position': 1}
            # Test valid create
            response = c.post(self.TASK_LIST_URL, data=data)
            assert response.status_code == 200
            task = self.get_json_response(response.data)
            assert 'uri' in task
            # Test invalid create
            del data['position']
            response = c.post(self.TASK_LIST_URL, data=data)
            assert response.status_code == 400

    def test_get_task(self):
        with app.test_request_context(), app.test_client() as c:
            c.get(self.TASK_LIST_URL)
            list_key = get_task_list_key()
            task = self.create_task(list_key)
            response = c.get(self.TASK_URL.format(task['task_id']))
            assert response.status_code == 200
            task = self.get_json_response(response.data)
            assert 'uri' in task
            response = c.get(self.TASK_URL.format('hittepå'))
            assert response.status_code == 404

    def test_delete_task(self):
        with app.test_request_context(), app.test_client() as c:
            c.get(self.TASK_LIST_URL)
            list_key = get_task_list_key()
            task = self.create_task(list_key)
            task_id = task['task_id']
            response = c.delete(self.TASK_URL.format(task_id))
            assert response.status_code == 204
            assert services.get_task(list_key, task_id) is None

    def test_update_task(self):
        with app.test_request_context(), app.test_client() as c:
            c.get(self.TASK_LIST_URL)
            list_key = get_task_list_key()
            # Test valid update
            data = {'task': 'testing stuff', 'position': 5}
            task = self.create_task(list_key, position=2)
            task_id = task['task_id']
            response = c.put(self.TASK_URL.format(task_id), data=data)
            assert response.status_code == 201
            updated_task = services.get_task(list_key, task_id)
            assert updated_task['position'] == data['position']

            # Test invalid update
            del data['task']
            response = c.put(self.TASK_URL.format(task_id), data=data)
            assert response.status_code == 400

if __name__ == '__main__':
    unittest.main()
