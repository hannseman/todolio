#!/usr/bin/env python
import os
from flask.ext.script import Manager, Shell, Server

from todolio.app import create_app
from todolio.settings import ProdConfig, DevConfig

if os.environ.get('TODOLIO_ENV') == 'prod':
    app = create_app(ProdConfig)
else:
    app = create_app(DevConfig)

manager = Manager(app)


def _make_context():
    """
    Return context dict for a shell session so you can access the app
    """
    return {'app': app}


@manager.command
def test():
    """
    Run the tests.
    """
    import pytest
    exit_code = pytest.main(['tests.py', '--verbose'])
    return exit_code


manager.add_command('runserver', Server())
manager.add_command('shell', Shell(make_context=_make_context))

if __name__ == '__main__':
    manager.run()
