from functools import wraps
from uuid import uuid4
from flask import Blueprint, request, render_template, redirect, url_for, session, current_app
from flask.ext.restful import abort, reqparse, fields, Resource, marshal_with
from ..extensions import api
from . import services

blueprint = Blueprint('task', __name__, static_folder='../static')


def get_task_list_key():
    """
    Get the task list key from session and if non existent generate a uuid prefixed by
    TASK_LIST_KEY_PREFIX and save it to session.
    """
    session_key = current_app.config['TASK_LIST_SESSION_KEY']
    task_list_key_prefix = current_app.config['TASK_LIST_KEY_PREFIX']
    todo_list_key = session.get(session_key)
    if not todo_list_key:
        todo_list_key = str(uuid4())
        session[session_key] = '{}:{}'.format(task_list_key_prefix, todo_list_key)
    return todo_list_key


def task_id_exists_or_404(func):
    """
    Decorator to check that the supplied argument task_id actually exists,
    if not return a 404.
    """
    @wraps(func)
    def wrapper(task_id, *args, **kwargs):
        list_key = get_task_list_key()
        if not services.task_exists(list_key, task_id):
            abort(404)
        else:
            return func(task_id, *args, **kwargs)
    return wrapper

# Arguments for Task and TaskList calls
parser = reqparse.RequestParser()
parser.add_argument('task', type=str, required=True)
parser.add_argument('position', type=int, required=True)
parser.add_argument('done', type=bool, default=False)

# Task fields to return to consumer
task_fields = {
    'task': fields.String,
    'position': fields.String,
    'done': fields.Boolean,
    'date_created': fields.String,
    'date_updated': fields.String,
    'uri': fields.Url('task'),
    'task_id': fields.String
}


class Task(Resource):
    method_decorators = [task_id_exists_or_404]

    @marshal_with(task_fields)
    def get(self, task_id):
        """
        Get a task
        """
        list_key = get_task_list_key()
        return services.get_task(list_key, task_id)

    def delete(self, task_id):
        """
        Delete a task
        """
        list_key = get_task_list_key()
        services.delete_task(list_key, task_id)
        return '', 204

    @marshal_with(task_fields)
    def put(self, task_id):
        """
        Update a task
        """
        args = parser.parse_args()
        task_data = {
            'task': args['task'],
            'position': args['position'],
            'done': args['done']
        }
        list_key = get_task_list_key()
        task = services.update_task(list_key, task_id, task_data)
        return task, 201


class TaskList(Resource):

    @marshal_with(task_fields)
    def get(self):
        """
        List all tasks
        """
        list_key = get_task_list_key()
        return sorted(services.list_tasks(list_key), key=lambda t: t['position'])

    @marshal_with(task_fields)
    def post(self):
        """
        Create a new task
        """
        args = parser.parse_args()
        task_data = {
            'task': args['task'],
            'position': args['position'],
            'done': args['done']
        }
        list_key = get_task_list_key()
        task = services.create_task(list_key, task_data)
        return task


api.add_resource(TaskList, '/tasks', endpoint='tasks')
api.add_resource(Task, '/tasks/<string:task_id>', endpoint='task')


@blueprint.route('/', methods=['GET'])
def base():
    """
    Base view to load tasks app
    """
    clear_tasks = 'clear' in request.args
    if clear_tasks:
        list_key = get_task_list_key()
        services.delete_tasks(list_key)
        return redirect(url_for('task.base'))

    return render_template('base.html')
