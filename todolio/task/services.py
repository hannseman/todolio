from uuid import uuid4
from datetime import datetime
from flask import json
from redis import Redis

encoding = 'utf-8'
redis = Redis(encoding=encoding)


def list_tasks(list_key):
    """
    Return a generator of all tasks stored in the hash :list_key
    """
    tasks = redis.hgetall(list_key)
    for task_id, task_json in tasks.items():
        task = json.loads(task_json.decode(encoding))
        task['task_id'] = task_id.decode(encoding)
        yield task


def get_task(list_key, task_id):
    """
    Get a task from redis by task_id
    """
    task_json = redis.hget(list_key, task_id)
    if not task_json:
        return
    task_data = json.loads(task_json.decode(encoding))
    task_data['task_id'] = task_id
    return task_data


def task_exists(list_key, task_id):
    """
    Check if task_id exists in redis
    """
    return bool(redis.hexists(list_key, task_id))


def create_task(list_key, task_data):
    """
    Save a new task in redis and set date_created and date_updated
    """
    task_id = str(uuid4())
    task_data['date_created'] = task_data['date_updated'] = datetime.now().isoformat()
    task_data['done'] = False
    task_data = json.dumps(task_data)
    redis.hset(list_key, task_id, task_data)
    return get_task(list_key, task_id)


def update_task(list_key, task_id, task_data):
    """
    Update an existing task in redis and update date_updated
    """
    task_data['date_updated'] = datetime.now().isoformat()
    task_data = json.dumps(task_data)
    redis.hset(list_key, task_id, task_data)
    return get_task(list_key, task_id)


def delete_task(list_key, task_id):
    """
    Delete a task from redis
    """
    return redis.hdel(list_key, task_id)


def delete_tasks(list_key):
    """
    Delete all tasks from redis
    """
    return redis.delete(list_key)
