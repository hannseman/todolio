from flask import Flask
from .settings import ProdConfig
from . import extensions
from . import task


def create_app(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_blueprints(app)
    register_extensions(app)
    return app


def register_extensions(app):
    extensions.api.init_app(app)


def register_blueprints(app):
    app.register_blueprint(task.views.blueprint)
