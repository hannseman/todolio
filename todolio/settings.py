import os

os_env = os.environ


class Config(object):
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    TASK_LIST_KEY_PREFIX = 'TASKS'
    TASK_LIST_SESSION_KEY = 'TASK_LIST'
    SECRET_KEY = None


class ProdConfig(Config):
    """Production configuration."""
    ENV = 'prod'
    DEBUG = False
    SECRET_KEY = os.environ.get('TODOLIO_SECRET_KEY')


class DevConfig(Config):
    """Development configuration."""
    ENV = 'dev'
    DEBUG = True
    SECRET_KEY = 'iw5vhc#0bsx+4p-)vnb+*u$r-5&)cwg4mbfjbj*vpvyu+1!j+_'


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    TASK_LIST_KEY_PREFIX = 'TASKS_TEST'
    SECRET_KEY = 'zwlslf-s_-@09mdudlp(0kd%83mouxl2bwvz^awte-dr0mc!xi'
