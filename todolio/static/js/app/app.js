/*global define*/
'use strict';

define([
    'angular',
    'angular-resource',
    'angular-sortable',
    'services',
    'controller'
], function (angular) {
    // Create the tasks app and inject services and controllers
    return angular.module('tasks', [
        'ngResource',
        'ui.sortable',
        'tasks.services',
        'tasks.controller'
    ]);
});
