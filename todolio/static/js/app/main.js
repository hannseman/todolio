/*global require*/
'use strict';

// Setup dependencies
require.config({
    // Alias libraries paths
    paths: {
        'angular': '../vendor/angular/angular',
        'angular-resource': '../vendor/angular-resource/angular-resource',
        'angular-sortable': '../vendor/angular-ui-sortable/sortable',
        'jquery': '../vendor/jquery/dist/jquery',
        'jqueryui': '../vendor/jquery-ui/jquery-ui'
    },

    // Angular does not support AMD out of the box, put it in a shim
    shim: {
        'angular': {
            exports: 'angular',
            deps: ['jquery']
        },
        'angular-resource': {
            deps: ['angular']
        },
        'angular-sortable': {
            deps: ['angular', 'jquery', 'jqueryui']
        }
    }

});

// Bootstrap angular and bind #task-app element with tasks app on document ready.
require(['angular', 'app'], function (angular) {

    angular.element(document).ready(function() {
        var task_app_el = angular.element('#task-app')[0];
        angular.bootstrap(task_app_el, ['tasks']);
    });

});
