/*global define*/
'use strict';

define(['angular', 'angular-resource'], function (angular) {
    var services = angular.module('tasks.services', ['ngResource']);

    // Declare Task service to map against the REST resource 'tasks'
    services.factory('Task', ['$resource', function ($resource) {
        return $resource('tasks/:task_id/', {task_id: '@task_id'}, {
                get:    {method:'GET'},
                save:   {method:'POST'},
                query:  {method:'GET', isArray:true},
                remove: {method:'DELETE'},
                delete: {method:'DELETE'},
                update: {'method': 'PUT'}
            }
        )
    }]);

    return services;
});
