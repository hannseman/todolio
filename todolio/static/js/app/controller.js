/*global define*/
'use strict';

define(['angular', 'services'], function (angular) {
    var controllers = angular.module('tasks.controller', ['tasks.services']);

    // Declare TaskController injected with Task-service and filter-function
    controllers.controller('TaskController', ['$scope', 'Task', 'filterFilter',
        function ($scope, Task, filterFilter) {
            // Get all tasks from service and put them in the controller scope
            var tasks = $scope.tasks = Task.query();

            // Add a container for adding a new task to the scope
            $scope.newTask = '';

            // Watch for changes to $scope.tasks and update the stats if any tasks was marked a done
            $scope.$watch('tasks', function (tasks) {
                $scope.tasksLeftCount = filterFilter(tasks, { done: false }).length;
            }, true);

            // Configure jQuery UI sortable
            $scope.sortableOptions = {
                stop: function () {
                    // Update tasks with their new positions after being sorted
                    tasks.forEach(function (task, index){
                        tasks[index].position = index + 1;
                        tasks[index].$update();
                    });
                },
                handle: '.handle',
                containment: '.tasks-list',
                placeholder: 'ui-state-highlight',
                distance: 30,
                tolerance: 'pointer',
                axis: 'y'
            };


            // Call to create a new task from newTask value and add it to $
            $scope.createTask = function () {
                var newTask = $scope.newTask.trim();
                if (!newTask.length) {
                    return;
                }
                Task.save({
                    task: newTask,
                    done: false,
                    position: tasks.length + 1
                }, function (task) {
                    tasks.push(task);
                    $scope.newTask = '';
                });

            };

            // Call to mark all tasks as done
            $scope.markAllDone = function () {
                tasks.forEach(function (task) {
                    task.done = true;
                    task.$update();
                });
            };

        }
    ]);

    return controllers;
});
